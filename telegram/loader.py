import logging

from aiogram import Bot, types, Dispatcher
from aiogram.contrib.fsm_storage.memory import MemoryStorage

from config import TOKEN
from telegram.utils.database.core import async_session

# from telegram.utils.database import DatabaseManager

logging.basicConfig(level=logging.INFO)

bot = Bot(
    token= TOKEN,
    # parse_mode=
)

storage = MemoryStorage()
dp = Dispatcher(bot, storage=storage)
# db = DatabaseManager('database.db')
db = async_session()