from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton

#В магазин
btn_shop = InlineKeyboardButton('В магазин', callback_data='shop')
keyboard_shop = InlineKeyboardMarkup().add(btn_shop)

#создание кнопок меню
btn_cancel = InlineKeyboardButton('Отмена', callback_data = 'cancel')
btn_genre = InlineKeyboardButton('К жанрам', callback_data= 'genre_game')
keyboard_menu = InlineKeyboardMarkup().add(btn_cancel, btn_genre)
btn_back = InlineKeyboardButton('Назад', callback_data= 'back')

#выбор жанра
btn_action = InlineKeyboardButton('Экшен', callback_data = 'action')
btn_mmo = InlineKeyboardButton('ММО', callback_data= 'mmo')
btn_rpg = InlineKeyboardButton('РПГ', callback_data= 'rpg')
keyboard_genre = InlineKeyboardMarkup().add(btn_action, btn_mmo).add(btn_rpg).add().add(btn_back)

#выбор игры
btn_witcher = InlineKeyboardButton('The Witcher 3', callback_data='witcher')
btn_gta = InlineKeyboardButton('Grand Theft Auto 5', callback_data='gta')
btn_mafia = InlineKeyboardButton('Mafia 2', callback_data='mafia')
btn_fallout = InlineKeyboardButton('Fallout 76', callback_data='fallout')
btn_oblivion = InlineKeyboardButton('The Elder Scrolls IV: Oblivion', callback_data='oblivion')
btn_bloodborne = InlineKeyboardButton('bloodborne', callback_data= 'bloodborne')

keyboard_games_action = InlineKeyboardMarkup().add(btn_gta, btn_mafia).add(btn_back)
keyboard_games_mmo = InlineKeyboardMarkup().add(btn_fallout, btn_oblivion).add(btn_back)
keyboard_games_rpg = InlineKeyboardMarkup().add(btn_witcher, btn_bloodborne).add(btn_back)

#способ оплаты
btn_sber = InlineKeyboardButton('Сбербанк', callback_data = 'sber')
btn_paymaster= InlineKeyboardButton('PayMaster', callback_data= 'paymaster')
keyboard_payment = InlineKeyboardMarkup().add(btn_sber, btn_paymaster).add(btn_back)

#В библиотеку игр
btn_library = InlineKeyboardButton('К библиотеке', callback_data = 'to_the_library')
keyboard_to_the_library = InlineKeyboardMarkup().add(btn_library, btn_shop)
keyboard_to_the_library2 = InlineKeyboardMarkup().add(btn_library)

#назад
keyboard_back = InlineKeyboardMarkup().add(btn_back)


