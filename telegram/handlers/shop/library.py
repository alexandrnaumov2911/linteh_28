import logging

from aiogram import types
from sqlalchemy import select

from telegram.handlers.shop.keyboards import keyboard_shop
from telegram.handlers.shop.state import InitialState
from telegram.loader import dp, db
from telegram.utils.database.model import UserGames


@dp.callback_query_handler(text = 'to_the_library',state=InitialState.initial)
async def to_the_library(callback: types.CallbackQuery):
    user_id = callback.from_user.id
    async with db as session:
        if user_obj := (await session.scalars(select(UserGames).filter_by(user_id=user_id).limit(1))).first():
            games = str((await session.scalars(select(UserGames.game).filter_by(user_id=user_id).limit(1))).first()).split(',\n')
            if len(games) == 6:
                games = '\n'.join(list(map(lambda x: f'✅{x}', games)))
                await callback.message.answer(f'Ваша библиотека игр:\n{games}\n'
                                              f'У вас полная коллекция игр')
            else:
                games = '\n'.join(list(map(lambda x:f'✅{x}', games)))
                await callback.message.answer(f'Ваша библиотека игр:\n{games}\n'
                                              f'Для того чтобы приобрести игру, нажмите на кнопку ниже, или вызовите команду - /shop', reply_markup=keyboard_shop)
        else:
            await callback.message.answer('В вашей библиотеке нет игр.\n'
                                          'Для того чтобы приобрести игру, нажмите на кнопку ниже, или вызовите команду - /shop', reply_markup=keyboard_shop)
        await callback.answer()

