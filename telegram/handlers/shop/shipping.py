from aiogram.types import ShippingOption, LabeledPrice

INSTANTLY_SHIPPING = ShippingOption(
    id='instantly',
    title='Сразу после оплаты',
    prices=[
        LabeledPrice(
            'Получить моментально', 200_00
        ),
    ]
)

TO_QUEUE_SHIPPING = ShippingOption(
    id='to_queue',
    title='В очередь',
    prices=[
        LabeledPrice(
            'Встать в очередь', 0
        ),
    ]
)