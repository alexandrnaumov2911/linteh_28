from aiogram.dispatcher.filters.state import StatesGroup, State

class InitialState(StatesGroup):
    initial = State()

class MenuState(StatesGroup):
    menu = State()
    choice_genre = State()
    choice_game = State()


class PaymentMethod(StatesGroup):
    choice_payment = State()
    payment = State()