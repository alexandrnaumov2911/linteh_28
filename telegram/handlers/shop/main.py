import logging

from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Text
from aiogram.types import PreCheckoutQuery, ContentType, LabeledPrice, ShippingQuery
from sqlalchemy import select, update


import telegram
from telegram.handlers.shop.keyboards import keyboard_menu, keyboard_genre, keyboard_payment, \
    keyboard_back, keyboard_games_rpg, keyboard_games_action, keyboard_games_mmo, keyboard_to_the_library, \
    keyboard_to_the_library2
from telegram.handlers.shop.shipping import TO_QUEUE_SHIPPING, INSTANTLY_SHIPPING
from telegram.loader import dp, db
from telegram.handlers.shop.state import MenuState, PaymentMethod, InitialState
from telegram.utils.database.model import UserGames


@dp.message_handler(commands=['shop', ], state='*')
async def command_menu(msg: types.Message, state: FSMContext):
    user_id = msg.from_user.id
    async with db as session:
        games = str((await session.scalars(select(UserGames.game).filter_by(user_id=user_id).limit(1))).first())
        length = len(games.split(',\n'))
        if length == 6:
            await msg.answer('Вы приобрели все игры', reply_markup=keyboard_to_the_library2)
            await state.set_state(InitialState.initial.state)
        else:
            await msg.answer('Добро пожаловать в магазин для покупки игр🎮,\n'
                         'В магазине вы можете приобрести игры следующих жанров:\n'
                         '✅Экшен\n'
                         '✅ММО\n'
                         '✅РПГ\n'
                         'Для того чтобы перейти к покупке игры, нажмите на кнопку - К жанрам', reply_markup=keyboard_menu)
            await state.set_state(MenuState.menu.state)
@dp.callback_query_handler(text = 'shop', state=InitialState.initial)
@dp.callback_query_handler(text = 'back', state=MenuState.choice_genre)
async def command_menu(callback: types.CallbackQuery, state: FSMContext):
    user_id = callback.from_user.id
    async with db as session:
        games = str((await session.scalars(select(UserGames.game).filter_by(user_id=user_id).limit(1))).first())
        length = len(games.split(',\n'))
        if length == 6:
            await callback.message.answer('Вы приобрели все игры', reply_markup=keyboard_to_the_library2)
            await state.set_state(InitialState.initial.state)
        else:
            await callback.message.answer('Добро пожаловать в магазин для покупки игр🎮,\n'
                             'В магазине вы можете приобрести игры следующих жанров:\n'
                             '✅Экшен\n'
                             '✅ММО\n'
                             '✅РПГ\n'
                             'Для того чтобы перейти к покупке игры, нажмите на кнопку - К жанрам',
                             reply_markup=keyboard_menu)
            await state.set_state(MenuState.menu.state)

@dp.callback_query_handler(text = 'cancel', state=MenuState.menu)
async def cancel(callback: types.CallbackQuery, state: FSMContext):
    await callback.message.edit_text('Если надумайте купить какую-то игру, нажмите на кнопку - В магазин, или '
                                     'вызовите команду - /shop', reply_markup=keyboard_to_the_library)
    await state.set_state(InitialState.initial.state)
    await callback.answer()

@dp.callback_query_handler(text = 'genre_game', state=MenuState.menu)
async def cancel(callback: types.CallbackQuery, state: FSMContext):
    await callback.message.answer('Каталог игр в разных жанрах\n'
                                     'Экшен:\n'
                                     '✅Grand Theft Auto 5 - 1199 руб.\n'
                                     '✅Mafia 2 - 799 руб.\n'
                                     'ММО:\n'
                                     '✅Fallout 76 - 1199 руб.\n'
                                     '✅The Elder Scrolls IV: Oblivion - 799 руб.\n'
                                     'РПГ:\n'
                                     '✅The Witcher 3: Wild Hunt - 1199 руб.\n'
                                     '✅Bloodborne - 1199 руб.\n'
                                     'Выберите подходящий вам жанр на кнопке ниже', reply_markup=keyboard_genre)
    await state.set_state(MenuState.choice_genre.state)
    await callback.answer()

@dp.callback_query_handler(text='back', state=MenuState.choice_game)
async def back_from_choice_game(callback: types.CallbackQuery, state: FSMContext):
    await callback.message.answer('Каталог игр в разных жанрах\n'
                                     'Экшен:\n'
                                     '✅Grand Theft Auto 5 - 1199 руб.\n'
                                     '✅Mafia 2 - 799 руб.\n'
                                     'ММО:\n'
                                     '✅Fallout 76 - 1199 руб.\n'
                                     '✅The Elder Scrolls IV: Oblivion - 799 руб.\n'
                                     'РПГ:\n'
                                     '✅The Witcher 3: Wild Hunt - 1199 руб.\n'
                                     '✅Bloodborne - 1199 руб.\n'
                                     'Выберите подходящий вам жанр на кнопке ниже', reply_markup=keyboard_genre)
    await state.set_state(MenuState.choice_genre.state)

@dp.callback_query_handler(text = 'back', state= PaymentMethod.choice_payment)
@dp.callback_query_handler(Text(equals=['action', 'mmo', 'rpg']), state=MenuState.choice_genre)
async def games(callback:types.CallbackQuery, state: FSMContext):
    if callback.data != 'back':
        genre_name = callback.data
        await state.update_data(genre_name = callback.data)
    else:
        genre_name = await state.get_data('genre_name')
        genre_name = genre_name.get('genre_name')
    await callback.message.edit_text('Выберите игру', reply_markup=keyboard_games_action if genre_name == 'action' else \
        keyboard_games_mmo if genre_name == 'mmo' else keyboard_games_rpg)
    await state.set_state(MenuState.choice_game)
    await callback.answer()


@dp.callback_query_handler(Text(equals = ['gta', 'mafia', 'fallout', 'oblivion', 'witcher', 'bloodborne']), state = MenuState.choice_game)
async def back_in_payment(callback: types.CallbackQuery, state: FSMContext):
    user_id = callback.from_user.id
    async with db as session:
        my_games = str((await session.scalars(select(UserGames.game).filter_by(user_id=user_id).limit(1))).first())
        name = callback.data
        name = get_name_game(name)
        if name in my_games:
            await callback.message.edit_text('Вы уже приобрели эту игру', reply_markup=keyboard_back)
        else:
            await callback.message.edit_text('Выберите способ оплаты', reply_markup=keyboard_payment)
            await state.update_data(name_game = callback.data)
            await state.set_state(PaymentMethod.choice_payment)


@dp.callback_query_handler(Text(equals=['sber', 'paymaster']), state= PaymentMethod.choice_payment)
async def payment(callback:types.CallbackQuery, state: FSMContext):
    name_game = await state.get_data('name_game')
    name_game = name_game.get('name_game')
    name_game = get_name_game(name_game)
    provider = callback.data
    gta5 = LabeledPrice(
        label='Grand Theft Auto 5',
        amount=1_199_00
    )
    mafia = LabeledPrice(
        label='Mafia 2',
        amount=799_00
    )
    fallout = LabeledPrice(
        label='Fallout 76',
        amount=1_199_00
    )
    oblivion = LabeledPrice(
        label='The Elder Scrolls IV: Oblivion',
        amount=799_00
    )
    witcher = LabeledPrice(
        label='The Witcher 3: Wild Hunt',
        amount=1_199_00
    )
    bloodborne = LabeledPrice(
        label='Bloodborne',
        amount=1_199_00
    )

    await callback.bot.send_invoice(
        chat_id=callback.from_user.id,
        title='Покупка',
        description=name_game,
        payload=name_game,
        provider_token=telegram.config.SBER if provider == 'sber' else telegram.config.PROVIDER_PAYMASTER_TOKEN,
        currency='RUB',
        start_parameter='test_bot',
        prices=[
            gta5 if name_game == 'Grand Theft Auto 5' else mafia if name_game == 'Mafia 2' \
            else fallout if name_game == 'Fallout 76' else oblivion \
            if name_game == 'The Elder Scrolls IV: Oblivion' else witcher \
            if name_game == 'The Witcher 3: Wild Hunt' else bloodborne

        ],
        is_flexible=True,
    )
    await state.set_state(PaymentMethod.payment.state)
    await callback.answer()

@dp.shipping_query_handler(state='*')
async def choose_shipping(shipping_query: ShippingQuery):
    await shipping_query.bot.answer_shipping_query(
        shipping_query_id=shipping_query.id,
        shipping_options=[
            INSTANTLY_SHIPPING,
            TO_QUEUE_SHIPPING,
        ],
        ok=True,
    )

@dp.pre_checkout_query_handler(state= PaymentMethod)
async def process_pre_checkout_query(pre_checkout_query: PreCheckoutQuery):
    await pre_checkout_query.bot.answer_pre_checkout_query(
        pre_checkout_query.id,
        ok=True,
    )

@dp.message_handler(content_types=ContentType.SUCCESSFUL_PAYMENT, state=PaymentMethod)
async def process_pay(msg: types.Message, state: FSMContext):
    shipping = msg.successful_payment.shipping_option_id
    user_id = msg.from_user.id
    name_game = await state.get_data('name_game')
    name_game = name_game.get('name_game')
    name_game = get_name_game(name_game)
    async with db as session:
        if user_obj := (await session.scalars(select(UserGames).filter_by(user_id=user_id).limit(1))).first():
            games = (await session.scalars(select(UserGames.game).filter_by(user_id=user_id).limit(1))).first()
            await session.execute(update(UserGames).where(UserGames.game == str(games)).values(game=games + ',\n' +  name_game))
            await session.commit()
        else:
            user_obj = UserGames(
                user_id=user_id,
                game = name_game
            )
            session.add(user_obj)
            await session.commit()
    if shipping == 'instantly':
        await msg.answer(f"Вы приобрели игру {name_game}, теперь она находится в вашей библиотеке. "
                         f"Если захотите снова приобрести игру, нажимайте на кнопку - В магазин, или вызовите команду - /shop", reply_markup=keyboard_to_the_library)
    else:
        await msg.answer(f"Вы приобрели игру {name_game}, когда до вас дойдёт очередь, с вами спишется менеджер и вы получите игру. "
                         f"Если захотите снова приобрести игру, нажимайте на кнопку - В магазин, или вызовите команду - /shop", reply_markup=keyboard_to_the_library)

    await state.set_state(InitialState.initial.state)

@dp.message_handler(state=InitialState)
@dp.message_handler(state=MenuState)
@dp.message_handler(state=PaymentMethod)
async def random_message(msg: types.Message):
    await msg.answer('Нажмите на одну из кнопок чтобы продолжить')

def get_name_game(name_game):
    name_game = 'Grand Theft Auto 5' if name_game == 'gta' else 'Mafia 2' if \
        name_game == 'mafia' else 'Fallout 76' if name_game == 'fallout' \
        else 'The Elder Scrolls IV: Oblivion' if name_game == 'oblivion' \
        else 'The Witcher 3: Wild Hunt' if name_game == 'witcher' else 'bloodborne'
    return name_game