from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup
from sqlalchemy import select

from telegram.handlers.shop.state import InitialState, StateInOpportunities
from telegram.loader import dp, db
from telegram.utils.database.model import Subscribers

btn_echo = InlineKeyboardButton('Эхо режим', callback_data='echo')
btn_converter = InlineKeyboardButton('Конвертор валют', callback_data='converter')
btn_game = InlineKeyboardButton('Камень, ножницы, бумага', callback_data='knb')

keyboard_for_basic = InlineKeyboardMarkup().add(btn_echo)
keyboard_for_premium = InlineKeyboardMarkup().add(btn_echo, btn_converter)
keyboard_for_vip = InlineKeyboardMarkup().add(btn_echo, btn_converter).add(btn_game)

@dp.callback_query_handler(text='opportunities', state=StateInOpportunities.echo)
@dp.callback_query_handler(text='opportunities', state=StateInOpportunities.converter)
@dp.callback_query_handler(text='opportunities', state=StateInOpportunities.knb)
@dp.callback_query_handler(text='opportunities', state=InitialState.initial)
async def opportunities(callback: types.CallbackQuery, state: FSMContext):
    user_id = callback.from_user.id
    async with db as session:
        if user_obj := (await session.scalars(select(Subscribers).filter_by(user_id=user_id, level_sub='basic').limit(1))).first():
            await callback.message.answer('На кнопках ниже показаны возможности бота на вашем уровне подписки\n'
            'Если вы хотите обновить уровень подписки вводите команду - /menu', reply_markup=keyboard_for_basic)
        elif user_obj := (await session.scalars(select(Subscribers).filter_by(user_id=user_id, level_sub='premium').limit(1))).first():
            await callback.message.answer('На кнопках ниже показаны возможности бота на вашем уровне подписки\n'
            'Если вы хотите обновить уровень подписки вводите команду - /menu', reply_markup=keyboard_for_premium)
        elif user_obj := (await session.scalars(select(Subscribers).filter_by(user_id=user_id, level_sub='vip').limit(1))).first():
            await callback.message.answer('На кнопках ниже показаны возможности бота на вашем уровне подписки\n'
            'C вашим уровнем подписки доступны все функции', reply_markup=keyboard_for_vip)
        else:
            await callback.message.answer('Для того чтобы пользоваться возможностями бота, необходим хотя бы базовый уровень подписки\n'
                                          'Если вы хотите обновить уровень подписки вводите команду - /menu')
        await state.set_state(InitialState.initial.state)
        await callback.answer()
