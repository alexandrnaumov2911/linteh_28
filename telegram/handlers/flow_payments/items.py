from dataclasses import dataclass
from typing import List

from aiogram.types import LabeledPrice


from telegram import config


@dataclass
class Item:
    title: str
    description: str
    start_parameter: str
    currency: str
    prices: List[LabeledPrice]
    provider_data: dict = None
    photo_url: str = None
    photo_size: int = None
    photo_width: int = None
    photo_height: int = None
    need_name: bool = False
    need_phone_number: bool = False
    need_email: bool = False
    need_shipping_address: bool = False
    send_phone_number_to_provider: bool = False
    send_email_to_provider: bool = False
    is_flexible: bool = False
    provider_token: str = config.PROVIDER_PAYMASTER_TOKEN

    def generate_invoices(self):
        return self.__dict__

CheapSkin = Item(
    title = 'Обычный предмет',
    description = (
        'Самый обычный скин, но добавит немного красок в вашу игру'
    ),
    currency = 'RUB',
    prices = [
        LabeledPrice(
            label='Обычный предмет',
            amount=200_00
        )
    ],
    start_parameter='create_invoice_cheap_skin',
    photo_url='https://yt3.googleusercontent.com/rHpRQe90U20MSB2u6tHa6kp-c0MW5GIeJuJGZFd2KabZRpEbhyH6aEzqlxn54tKncR99Lk_n=s900-c-k-c0x00ffffff-no-rj',
    photo_size=500,
    need_shipping_address = True
)

RareSkin = Item(
    title = 'Редкий предмет',
    description = (
        'Редкий скин, добавит красок в вашу игру, но вы будете часто видеть такой же скин как и у вас'
    ),
    currency = 'RUB',
    prices = [
        LabeledPrice(
            label='Редкий предмет',
            amount=4_000_00
        )
    ],
    start_parameter='create_invoice_cheap_skin',
    photo_url='https://catherineasquithgallery.com/uploads/posts/2021-03/1614584691_37-p-standoff-na-belom-fone-43.png',
    photo_size=500,
    need_shipping_address = True
)

LegendarySkin = Item(
    title = 'Легендарный предмет',
    description = (
        'Самый редкий скин, обладать им большая редкость, очень красочный, ваши соперники будут вам завидовать)'
    ),
    currency = 'RUB',
    prices = [
        LabeledPrice(
            label='Легендарный предмет',
            amount=50_000_00
        )
    ],
    start_parameter='create_invoice_cheap_skin',
    photo_url='https://cfire.ru/pictures/content/updates/2017/05/ak47cerberus.png',
    photo_size=500,
)