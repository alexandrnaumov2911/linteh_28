from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup

btn_cheap_skin = InlineKeyboardButton('Обычный скин', callback_data= 'regular_skin')
btn_rare_skin = InlineKeyboardButton('Редкий скин', callback_data='rare_skin')
btn_very_rare_skin = InlineKeyboardButton('Легендарный скин', callback_data='very_rare_skin')

kb_skins = InlineKeyboardMarkup().add(
    btn_cheap_skin,
    btn_rare_skin,
    btn_very_rare_skin
)