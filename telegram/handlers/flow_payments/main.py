from aiogram.types import Message, ShippingQuery, PreCheckoutQuery, CallbackQuery, ContentType

from telegram.handlers.flow_payments.items import CheapSkin, RareSkin, LegendarySkin
from telegram.handlers.flow_payments.shipping import PICKUP_SHIPPING, POST_FAST_SHIPPING, POST_REGULAR_SHIPPING
from telegram.handlers.flow_payments.keyboard_skins import kb_skins

from telegram.loader import dp

@dp.message_handler(commands='skins')
async def show_invoices(msg: Message):
    await msg.answer('Выберите какой скин вы хотите?', reply_markup=kb_skins)

@dp.callback_query_handler(text = 'regular_skin')
async def show_invoices(cb: CallbackQuery):
    await cb.bot.send_invoice(cb.from_user.id, **CheapSkin.generate_invoices(), payload='1')

@dp.callback_query_handler(text = 'rare_skin')
async def show_invoices(cb: CallbackQuery):
    await cb.bot.send_invoice(cb.from_user.id, **RareSkin.generate_invoices(), payload='2')

@dp.callback_query_handler(text = 'very_rare_skin')
async def show_invoices(cb: CallbackQuery):
    await cb.bot.send_invoice(cb.from_user.id, **LegendarySkin.generate_invoices(), payload='3')

@dp.shipping_query_handler()
async def choose_shipping(shipping_query: ShippingQuery):
    await shipping_query.bot.answer_shipping_query(
        shipping_query_id=shipping_query.id,
        shipping_options=[
            POST_REGULAR_SHIPPING,
            POST_FAST_SHIPPING,
            PICKUP_SHIPPING,
        ],
        ok=True,
    )



@dp.pre_checkout_query_handler()
async def process_pre_checkout_query(pre_checkout_query: PreCheckoutQuery):
    await pre_checkout_query.bot.answer_pre_checkout_query(pre_checkout_query_id=pre_checkout_query.id, ok=True)
    await pre_checkout_query.bot.send_message(chat_id=pre_checkout_query.from_user.id, text='Спасибо за покупку')