from aiogram import types
from aiogram.dispatcher import FSMContext


from telegram.handlers.shop.keyboards import keyboard_to_the_library
from telegram.handlers.shop.state import InitialState
from telegram.loader import dp



@dp.message_handler(commands= ['start', ], state = '*')
async def command_start(msg: types.Message, state: FSMContext):
    await msg.answer('Привет, это бот для покупки игр.🎮\n '
                     'Для того чтобы приобрести игру, '
                     'необходимо перейти по кнопке - В магазин, или вызвать команду - /shop.\n'
                     'Для того чтобы перейти к своей игровой библиотеке нажмите кнопку - К библиотеке', reply_markup=keyboard_to_the_library)
    await state.set_state(InitialState.initial)
