from telegram.handlers.shop.state import InitialState, StateInOpportunities
from telegram.handlers.shop.keyboards import keyboard_menu_opportunities
from telegram.loader import dp

import requests
from aiogram import types
from aiogram.dispatcher import FSMContext

@dp.callback_query_handler(text = 'converter', state=InitialState.initial)
async def currency_convert(callback: types.CallbackQuery, state: FSMContext):
    await callback.message.answer('Добро пожаловать в конвертер валют, он способен переводить рубли в юани. '
                     'Для того чтобы пользоваться конвертером, достаточно вводить количество рублей, '
                     'которое хотите перевести в юани. Если захотите вернуться к возможностям бота, нажмите на кнопку ниже', reply_markup=keyboard_menu_opportunities)
    await state.set_state(StateInOpportunities.converter.state)
    await callback.answer()

@dp.message_handler(state= StateInOpportunities.converter)
async def end(msg:types.Message, state: FSMContext):
    response = requests.get("https://www.cbr-xml-daily.ru/daily_json.js").json()['Valute']['CNY']['Value']
    if msg.text.isdigit():
        await state.update_data(
            answer = msg.text
        )
        await msg.answer(
            "Итог:\n"
            f"{msg.text} RUB ---> {int(msg.text) * response} CNY", reply_markup= keyboard_menu_opportunities
        )
    else:
        await msg.answer('Введите количество рублей корректно(только число)', reply_markup=keyboard_menu_opportunities)
