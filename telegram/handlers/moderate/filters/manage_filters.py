from aiogram.dispatcher.filters import BoundFilter
from aiogram.types import CallbackQuery, Message

class IsGroup(BoundFilter):
    async def check(self, obj: Message | CallbackQuery, *args):
        if isinstance(obj, Message):
            return obj.chat.id < 0
        return obj.message.chat.id < 0