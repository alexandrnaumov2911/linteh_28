import asyncio
import datetime
import re

from aiogram import types
from aiogram.dispatcher.filters import AdminFilter, Command
from aiogram.utils.exceptions import BadRequest

from telegram.handlers.moderate.filters.manage_filters import IsGroup
from telegram.loader import dp


@dp.message_handler(IsGroup(), Command('ro', prefixes='!/'), AdminFilter())
async def read_only_mode(msg: types.Message):
    member = msg.reply_to_message.from_user
    command_parse = re.compile(r'(!ro|/ro) ?(\d+)? ?([a-zA-Z ])+?')
    parsed = command_parse.match(msg.text)
    time = parsed.group(2)
    comment = parsed.group(3)
    if not time:
        time = 5
    else:
        time = int(time)
    until_date = datetime.datetime.now() + datetime.timedelta(minutes=time)
    ReadOnlyPermission = types.ChatPermissions(
        can_send_messages=False,
        can_send_media_messages=False,
        can_send_polls=False,
        can_pin_messages=False,
        can_invite_users=True,
        can_change_info=False,
        can_add_web_page_previews=False,
    )
    try:
        await msg.bot.restrict_chat_member(
            msg.chat.id,
            user_id=member.id,
            permissions=ReadOnlyPermission,
            until_date=until_date
        )
        await msg.answer(
            f'Пользователю {member.get_mention(as_html=True)} запрещено писать на {time} минут по причине {comment}')
    except BadRequest:

        await msg.answer('Пользователь является администратором')
        service_message = await msg.reply('Сообщение удалится через 5 секунд')
        await asyncio.sleep(5)
        await msg.delete()
        await service_message.delete()