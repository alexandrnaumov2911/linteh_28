from asyncio import sleep
from random import choice

from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Text

from telegram.handlers.shop.keyboards import keyboard_menu_opportunities
from telegram.handlers.shop.state import InitialState, StateInOpportunities
from telegram.loader import dp

values = ['ножницы', 'бумага', 'камень']

list_win = [
    ['ножницы', 'бумага'],
    ['бумага', 'камень'],
    ['камень', 'ножницы']
]
@dp.callback_query_handler(text='knb', state=InitialState.initial)
async def knb(callback: types.CallbackQuery, state: FSMContext):
    await callback.message.answer("Добро пожаловать в игру камень, ножницы бумага.\n"
                                  "Для того что бы начать, напишите свой вариант(ножницы, бумага или камень)\n"
                                  "Если хотите вернуться к другим возможностям бота, нажмите кнопку ниже", reply_markup=keyboard_menu_opportunities)
    await state.set_state(StateInOpportunities.knb)
    await callback.answer()

@dp.message_handler(Text(equals=["камень", "ножницы", "бумага",], ignore_case= True), state=StateInOpportunities)
async def result(msg: types.Message):
    win = (
        'Бот победил\n'
        'Для того чтобы продолжить напишите свой(ножницы, бумага или камень)'
    )
    lose = (
        'Вы победили\n'
        'Для того чтобы продолжить напишите свой(ножницы, бумага или камень)'
    )
    draw = (
        'Ничья\n'
        'Для того чтобы продолжить напишите свой(ножницы, бумага или камень)'
    )

    res = list()
    res.append(choice(values))
    res.append(msg.text)
    await sleep(1)
    await msg.answer(f'Выбор бота: {res[0]}')
    await msg.answer(win if res in list_win else draw if res[0] == res[1] else lose, reply_markup=keyboard_menu_opportunities)
@dp.message_handler(state=StateInOpportunities)
async def random_message(msg: types.Message):
    await msg.answer('Введите корректное значение(камень, ножницы или бумага)')