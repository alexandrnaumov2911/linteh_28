from aiogram import types
from aiogram.dispatcher import FSMContext

from telegram.handlers.shop.keyboards import keyboard_menu_opportunities
from telegram.handlers.shop.state import InitialState, StateInOpportunities
from telegram.loader import dp

@dp.callback_query_handler(
    text = 'echo', state=InitialState.initial
)
async def echo_start(callback: types.CallbackQuery, state: FSMContext):
    await callback.message.answer('Вы включили эхо режим, напишите любое сообщение, '
                                  'а я повторю', reply_markup=keyboard_menu_opportunities)
    await state.set_state(StateInOpportunities.echo.state)

@dp.message_handler(
    state=StateInOpportunities.echo, content_types=types.ContentType.TEXT
)
async def echo_text(msg: types.Message):
    await msg.answer(msg.text, reply_markup=keyboard_menu_opportunities)
