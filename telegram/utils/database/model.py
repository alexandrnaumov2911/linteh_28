from typing import Optional, List

from sqlalchemy import String, Column, Integer, BigInteger, ForeignKey
from sqlalchemy.orm import DeclarativeBase, relationship, backref


class Base(DeclarativeBase):
    id = Column(Integer(), primary_key=True)

class User(Base):
     __tablename__ = "user_accounts"

     user_id = Column(BigInteger(), unique=True)
     username = Column(String(30), )

     def __repr__(self):
         return f'{self.user_id}'

class Msg_User(Base):
    __tablename__ = "message_from_user"

    user = relationship('User', backref=backref('message_from_user', cascade='all, delete-orphan', ))
    user_id = Column(BigInteger(), ForeignKey('user_accounts.user_id'))
    chat_id = Column(BigInteger(), )
    message_text = Column(String(500), )

    def __repr__(self):
        return f'{self.user_id} написал - {self.message_text}'

class UserGames(Base):
    __tablename__ = "user_games"
    user = relationship('User', backref=backref('subscribers_users', cascade='all, delete-orphan', ))
    user_id = Column(BigInteger(), ForeignKey('user_accounts.user_id'))
    game = Column(String(), )

    def __repr__(self):
        return f'{self.user_id} - {self.game}'







