from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.ext.asyncio import create_async_engine, async_sessionmaker

__credentional = {
    'db': "innopolis",
    'user': "docker",
    'password': "docker",
    'host': "localhost",
    'post': 5432,
}

engine = create_async_engine('postgresql+asyncpg://{user}:{password}@{host}:{post}/{db}'.format(**__credentional), echo=False)
async_session = async_sessionmaker(
    engine, class_=AsyncSession, expire_on_commit=False
)