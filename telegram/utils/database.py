import sqlite3

class DatabaseManager:

    def __init__(self, path):
        self.path = path

    def __enter__(self):
        self.conn = sqlite3.connect(self.path)
        self.conn.execute('pragma foreign_keys = on')
        self.conn.commit()
        self.cur = self.conn.cursor()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.conn:
            self.conn.close()

    def create_tables(self):
        self.query('CREATE TABLE IF NOT EXISTS user (user_id INT PRIMARY KEY, chat_id INT, username TEXT)')
        self.query('CREATE TABLE IF NOT EXISTS messages_from_users (num INTEGER PRIMARY KEY AUTOINCREMENT, user_id INT, chat_id INT,  message TEXT)')
        self.query('CREATE TABLE IF NOT EXISTS block_list (user_id INTEGER PRIMARY KEY, timestamp INTEGER, reason TEXT)')
        self.query('CREATE TABLE IF NOT EXISTS complaint_book (user_id INT, timestamp INTEGER, username TEXT, reason TEXT)')
        self.query('CREATE TABLE IF NOT EXISTS  polling_favorite_film (id INT PRIMARY KEY, user_id INTEGER, vote INTEGER check (vote in (0, 1)), data TEXT)')

    def query(self, arg, values=None):
        if values is None:
            self.cur.execute(arg)
        else:
            self.cur.execute(arg, values)
        self.conn.commit()

    def fetchone(self, arg, values=None):
        if values is None:
            self.cur.execute(arg)
        else:
            self.cur.execute(arg, values)
        return self.cur.fetchone()

    def fetchall(self, arg, values=None):
        if values is None:
            self.cur.execute(arg)
        else:
            self.cur.execute(arg, values)
        return self.cur.fetchall()