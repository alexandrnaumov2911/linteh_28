import os

from aiogram import types

ADMINS = [747184261,]
TOKEN = os.getenv('TOKEN')
NGROK = os.getenv('NGROK', '')
PROVIDER_PAYMASTER_TOKEN = os.getenv('PROVIDER_PAYMASTER_TOKEN', '')
SBER = os.getenv('SBER', '')

WEBHOOK_PATH = ''
WEBHOOK_URL = f'{NGROK}{WEBHOOK_PATH}'


WEBAPP_HOST = '127.0.0.1'  # or ip
WEBAPP_PORT = 8000

if not TOKEN:
    exit('Error: no token provided')

COMMANDS = [
    types.BotCommand('start', 'Стартуем!'),
    types.BotCommand('shop', 'К магазину игр')
]
