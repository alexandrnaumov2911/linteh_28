import logging
import sqlite3
from types import NoneType

from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.handler import CancelHandler
from aiogram.dispatcher.middlewares import BaseMiddleware
from aiogram.types import Update
from sqlalchemy import select

from telegram.loader import db
from telegram.utils.database.model import User, Msg_User


class SomeMiddleware(BaseMiddleware):
    async def on_pre_process_message(self, message: types.Message, data: dict):

        if message is not None:
            user_id = message.from_user.id
            username = message.from_user.username
            async with db as session:
                if user_obj := (await session.scalars(select(User).filter_by(user_id=user_id).limit(1))).first() is None:
                    user_account = User(
                        user_id=user_id,
                        username=username
                    )
                    session.add(user_account)
                    await session.commit()

    async def on_process_message(self, message: types.Message, data: dict):
        if message is not None:
            user_id = message.from_user.id
            chat_id = message.message_id
            message_text = message.text
            async with db as session:
                message_obj = Msg_User(
                    user_id = user_id,
                    chat_id = chat_id,
                    message_text = message_text,
                )
                session.add(message_obj)
                await session.commit()
